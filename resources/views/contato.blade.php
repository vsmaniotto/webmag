@extends('layout.site')

@section('titulo', 'Contato')

@section('conteudo')

    <div class="row">
        <div class="col-lg-12">
            <h2 class="h2">Contato</h2>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-lg-8">
            <form action="#">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nome...">
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control" placeholder="Telefone...">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="email">
                </div>
                <div class="form-group">
                    <textarea name="txtMensagem" rows="5" class="form-control" placeholder="Mensagem?"></textarea>
                </div>
                <div class="text-left">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
            
        </div>
        <div class="col-lg-4 text-left">
           <div>
                <h5 class="h5">Marília:</h5>
            Av. Sampaio Vidal, 587 <br>
            Tel.: (14) 91292-0991  <br>
            Email: support@mysite.com
           </div>
           <div class="mt-5">
               <h5 class="h5">São Paulo</h5>
               Company Inc. <br>
               Av. Paulista, 201 <br>
               Centro, SP <br>
               Tel.: (11) 1478-2587 <br>
               usa@mysite.com
           </div>


            
            
        </div>
    </div>
    
@endsection