<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/footer.css">
    <title>WEBMAG - @yield('titulo') </title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <header>
                    <img src="img/logo.png" alt="Logo">
                    <nav class="navbar navbar-expand-lg">
                        <ul class="navbar-nav mx-auto ">
                            <li class="nav-item"><a href="{{route('home')}}" class="nav-link">HOME</a></li>
                            <li class="nav-item"><a href="{{route('html')}}" class="nav-link">HTML</a></li>
                            <li class="nav-item"><a href="{{route('javascript')}}" class="nav-link">JAVASCRIPT</a></li>
                            <li class="nav-item"><a href="{{route('dicascss')}}" class="nav-link">PLANEJAMENTO</a></li>
                            <li class="nav-item"><a href="{{route('videos')}}" class="nav-link">VÍDEO AULAS</a></li>
                            <li class="nav-item"><a href="{{route('contato')}}" class="nav-link">CONTATO</a></li>
                        </ul>
                    </nav>
                    <hr>
                </header>
                <main>
                    @yield('conteudo')
                </main>
                <hr>
                <footer>
                    <p>Todos os direitos são reservados</p>
                </footer>
            </div>
        </div>
    </div>
</body>
</html>