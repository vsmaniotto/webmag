@extends('layout.site')
@section('titulo', 'Videoaulas')

@section('conteudo')
    <div class="row mt-4 mb-5">
        <div class="col-lg-12">
            <h2 class="h2"> Videoaulas</h2>
        </div>
    </div>
    <div class="row">
        
        <div class="col-lg-4">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-lg-7 text-left ">
            <h3 class="h3">Cursode HTML e CSS para iniciantes - Aula 1 </h3>
            <p>Nesta primeira aula vou passar o conceito básico de HTML e mostrar como criar sua primeira página para teste.Comesta página você vai aprender a criar a estrutura mais simples de um documentoHTML, definir títulos e também a codificação (charset) para seus documentos.</p>
        </div>

    </div>
    <div class="row mt-5 mb-4 text-left">
           <div class="col-lg-4">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/ze9--J4PJ_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4 class="h4">GitHub, GitLab ou Bitbucket? Qual nós usamos! // CAC #6</h3>
           </div>
           <div class="col-lg-4">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/jyTNhT67ZyY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4 class="h4">MVC // Dicionário do Programador</h4>
           </div>
           <div class="col-lg-4">
                <iframe width="100%" height="200" src="https://www.youtube.com/embed/5K7OGSsWlzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <h4 class="h4">O que um Analista de Sistemas faz? // Vlog #77</h4>
           </div>
    </div>
    
@endsection
    
