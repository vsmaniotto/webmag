@extends('layout.site')

@section('titulo', 'Home')

@section('conteudo')
    

<div class="row">
        <div class="col-lg-4">
            <img src="img/post-3.jpg" alt="post-3" class="img-fluid">
            <h3 class="h3">Dicas de HTML</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit deleniti, minima, saepe quam accusamus quia distinctio
                asperiores quasi ipsum quas adipisci tempore laborum ex quibusdam itaque omnis dolores maiores eveniet!</p>
        </div>
        <div class="col-lg-4">
            <img src="img/post-4.jpg" alt="post-4" class="img-fluid">
            <h3 class="h3">O que é MVC?</h3>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Autem odit quasi obcaecati quidem, aliquam, praesentium
                corrupti tempora modi dolorem, dicta ea illum laboriosam. Ducimus recusandae placeat aliquam omnis voluptatem
                nam!</p>
        </div>
        <div class="col-lg-4">
            <img src="img/post-5.jpg" alt="post-5" class="img-fluid">
            <h3 class="h3">Planejamento</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos amet non vel iure, est excepturi. In, labore nam.
                Dicta odio quas delectus aspernatur repellat voluptatem provident voluptas accusamus! Eligendi, officiis.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 mx-auto text-center">
            <h2 class="h2">FAÇA DIFERENTE...</h2>
            <p id="diferente">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus atque dolor fugiat incidunt cum quis neque officia
                quas at? Totam optio quod earum? Ipsa maiores dolor cum aliquid, recusandae harum. Lorem ipsum dolor sit
                amet consectetur adipisicing elit. Temporibus molestias, enim dolorem laboriosam odio, fuga incidunt eveniet
                autem tempore dolor et tempora. Quo adipisci ducimus ipsam explicabo cumque, sed aliquid.</p>
        </div>
    </div>
    @endsection