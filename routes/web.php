<?php
// use Symfony\Component\Routing\Annotation\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('html', function(){
    return view('html');
})->name('html');

Route::get('javascript', function(){
    return view('javaScript');
})->name('javascript');

Route::get('dicascss', function(){
    return view('dicasCSS');
})->name('dicascss');

Route::get('videos', function(){
    return view('videos');
})->name('videos');

Route::get('contato', function(){
    return view('contato');
})->name('contato');